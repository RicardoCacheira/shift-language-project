class Color {
    constructor(hex = "000000") {
        this.hex = hex;

    }
}

const BasicColors = {
    WHITE: new Color("FFFFFF"),
    BLACK: new Color("000000"),
    RED: new Color("FF0000"),
    GREEN: new Color("00FF00"),
    BLUE: new Color("0000FF"),
    YELLOW: new Color("FFFF00"),
    MAGENTA: new Color("FF00FF"),
    CYAN: new Color("00FFFF"),
}

class TokenError {
    constructor(msg, startPos, endPos) {
        this.msg = msg;
        this.startPos = startPos;
        this.endPos = endPos;
    }
}

class RuntimeError {
    constructor(msg, code) {
        this.msg = msg;
        this.code = code;
    }
}