class WebInterpreter {
    constructor(parent, consoleContainer, consoleX, consoleY) {
        this.theme = {
            DEFAULT: new Color("A9B7C6"),
            INFO: new Color("A8C023"),
            ERROR: new Color("C93B48"),
            BACKGROUND: new Color("2B2B2B"),
            VARIABLE: new Color("FFC66D"),
            STRING: new Color("6A8759"),
            NUMBER: new Color("6897BB"),
            MODIFIERS: new Color("CC7832"),
            SPECIAL: new Color("9876AA"),
            COMMENT: new Color("808080"),
        }
        this.parent = parent;
        this.console = new WebConsole(consoleX, consoleY, consoleContainer, this.theme);
        this.engine = new I4Engine();
        this.engine.addOutput(this);
        this.engine.addInputSystem({
            requestInput(){
                return window.prompt("Input");
            }
        })
        this.engine.loadGlobalModules();
    }

    processOutput(output, type, custom) {
        switch (type) {
            case OutputType.PLAIN:
                this.console.writeOnBuffer(output, this.theme.DEFAULT);
                break;
            case OutputType.INFO:
                this.console.writeOnBuffer(output, this.theme.INFO);
                break;
            case OutputType.ERROR:
                this.console.writeOnBuffer(output, this.theme.ERROR);
                break;
            case OutputType.CUSTOM:
                this.console.writeOnBuffer(output, new Color(custom));
                break;
        }
        this.console.renderBuffer();
    }

}

function startInterpreter(title, parent) {
    var table = document.createElement("table");

    var tr = document.createElement("tr");
    var th = document.createElement("th");
    th.innerText = title;
    tr.appendChild(th);
    table.appendChild(tr);

    tr = document.createElement("tr");
    var outputField = document.createElement("td");
    tr.appendChild(outputField);
    table.appendChild(tr);

    tr = document.createElement("tr");
    var inputLine = document.createElement("td");
    inputLine.innerText = "Command:";
    var textField = document.createElement("input");
    textField.setAttribute("type", "text");
    inputLine.appendChild(textField);

    var button = document.createElement("input");
    button.textField = textField;
    button.setAttribute("type", "button");
    button.setAttribute("onclick", "this.interpreter.engine.input(this.textField.value);this.textField.value=''");
    button.value = "Run";
    inputLine.appendChild(button);
    tr.appendChild(inputLine);
    table.appendChild(tr);

    parent.appendChild(table);
    parent.interpreter = new WebInterpreter(parent, outputField, 80, 24);
    button.interpreter = parent.interpreter;
    parent.interpreter.console.startConsole();

    createCodeEditor(parent, parent.interpreter.engine.parser, parent.interpreter.theme);

}

//TODO Fix chosing text with keyboard and remove extra
function createCodeEditor(parent, parser, theme) {

    var table = document.createElement("table");

    var tr = document.createElement("tr");
    var th = document.createElement("th");
    th.setAttribute("colspan", "2");
    th.innerText = "Code Editor";
    tr.appendChild(th);
    table.appendChild(tr);

    tr = document.createElement("tr");
    var inputField = document.createElement("td");
    inputField.setAttribute("contenteditable", "true");
    tr.appendChild(inputField);
    table.appendChild(tr);
//TODO Fix This
    inputField.outputField = inputField;
    inputField.inputField = inputField;
    inputField.parser = parser;
    inputField.theme = theme;
    inputField.highlightTokens = highlightTokens;
    inputField.getCaretPos = getCaretPos;
    inputField.setCaretPos = setCaretPos;
    inputField.highlightTokens = highlightTokens;
    inputField.addEventListener("keyup", function () {
        console.log("Position");

        var pos = this.getCaretPos(this);
        console.log(pos);
        this.highlightTokens();
        this.setCaretPos(this, pos);
    });
    tr = document.createElement("tr");
    var td = document.createElement("td");
    td.setAttribute("colspan", "2");
    inputField.errorField = td;
    tr.appendChild(td);
    table.appendChild(tr);

    parent.appendChild(table);
}

//TODO Fix use of the parser
function highlightTokens() {
    this.errorField.innerHTML = '';
    var src = this.inputField.innerText;
    console.log(src)
    var result = this.parser.lexer.analyze(src);
    var out = this.outputField;
    out.innerText = '';
    if (result instanceof TokenError) {
        writeWithColor(this.errorField, result.msg, this.theme.ERROR);
        writeWithColor(out, src.substring(0, result.startPos), this.theme.INFO);
        writeWithColor(out, src.substring(result.startPos, result.endPos), this.theme.ERROR);
        writeWithColor(out, src.substring(result.endPos), this.theme.INFO);
    } else {
        //Its a different var than result because its only used to check parser errors, not color tokens.
        var parseResult = this.parser.parse(src);
        if (parseResult instanceof TokenError) {
            writeWithColor(this.errorField, parseResult.msg, this.theme.ERROR);
            writeWithColor(out, src.substring(0, parseResult.startPos), this.theme.INFO);
            writeWithColor(out, src.substring(parseResult.startPos, parseResult.endPos), this.theme.ERROR);
            writeWithColor(out, src.substring(parseResult.endPos), this.theme.INFO);
        } else {
            var length = result.length;
            if (length > 0) {
                writeWithColor(out, src.substring(0, result[0].startPos), this.theme.DEFAULT);
                for (var i = 0; i < length - 1; i++) {
                    writeToken(out, src, result[i], this.theme);
                    writeWithColor(out, src.substring(result[i].endPos, result[i + 1].startPos), this.theme.DEFAULT);
                }
                writeToken(out, src, result[length - 1], this.theme);
                writeWithColor(out, src.substring(result[length - 1].endPos), this.theme.DEFAULT);
            }
            else {
                writeWithColor(out, src, this.theme.DEFAULT);
            }
        }
    }
}

function getCaretPos(element) {
    var caretOffset;
    var range = window.getSelection().getRangeAt(0);
    var preCaretRange = range.cloneRange();
    preCaretRange.selectNodeContents(element);
    preCaretRange.setEnd(range.endContainer, range.endOffset);
    caretOffset = preCaretRange.toString().length;
    return caretOffset;
}

//TODO Fix this code
function setCaretPos(element, pos) {

    var range = document.createRange();
    var sel = window.getSelection();
    var offset = 0;
    var list = element.childNodes;
    var found = false;
    for (var i = 0; i < list.length && !found; i++) {
        var children = list[i];
        for (var j = 0; j < children.childNodes.length && !found; j++) {
            var textNode = children.childNodes[j];
            if (textNode) {
                var text = textNode.data;
                // console.log("Offset: " + offset);
                // console.log(text);
                // console.log(text.length);
                if (pos > offset + text.length) {
                    offset += text.length
                    //     console.log("Offset After: " + offset);
                } else {
                    //    console.log(textNode);
                    if (textNode.nodeName == "BR") {
                        range.setStartAfter(textNode);
                    } else {
                        range.setStart(textNode, pos - offset);
                    }
                    sel.removeAllRanges();
                    sel.addRange(range);
                    element.focus();
                    found = true;
                }
            }
        }
    }
}


function writeToken(parent, src, token, theme) {
    var color;
    switch (token.type) {
        case TokenTypeList.ADD_VAR:
        case TokenTypeList.REM_VAR:
        case TokenTypeList.POP_VAL_FROM:
        case TokenTypeList.PUSH_VAL_INTO:
        case TokenTypeList.SET_VAR:
        case TokenTypeList.SET_COMMAND:
        case TokenTypeList.COMMAND_END:
        case TokenTypeList.COMMA:
        case TokenTypeList.RETURN:
        case TokenTypeList.BREAK:
            color = theme.MODIFIERS;
            break;
        case TokenTypeList.STRING:
            color = theme.STRING;
            break;
        case TokenTypeList.NUMBER:
            color = theme.NUMBER;
            break;
        case TokenTypeList.VARIABLE:
            color = theme.VARIABLE;
            break;
        case TokenTypeList.TRUE:
        case TokenTypeList.FALSE:
            color = theme.SPECIAL;
            break;
        case TokenTypeList.COMMENT:
            color = theme.COMMENT;
            break;
        default:
            color = theme.DEFAULT;
    }
    writeWithColor(parent, src.substring(token.startPos, token.endPos), color);
}

function writeWithColor(parent, text, color) {
    if (text.length > 0) {
        var colorElement = window.document.createElement("span");
        colorElement.setAttribute("style", "color: #" + color.hex);
        var textNode = window.document.createTextNode(text);
        colorElement.appendChild(textNode);
        parent.appendChild(colorElement);
    }
}

