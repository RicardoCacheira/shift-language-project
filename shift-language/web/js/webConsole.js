class WebConsole {
    constructor(xSize, ySize, outputElement, theme) {
        this.xSize = xSize;
        this.ySize = ySize;
        this.posX = 0;
        this.posY = 0;
        this.outputElement = outputElement;
        this.theme = theme;
        this.startBuffers();
    }

    startConsole() {
        this.startBuffers();
        this.renderBuffer();
    }

    startBuffers() {
        this.posX = 0;
        this.posY = 0;
        this.charBuffer = [];
        this.colorBuffer = [];
        for (var i = 0; i < this.ySize; i++) {
            var lineA = [];
            var lineB = [];
            for (var j = 0; j < this.xSize; j++) {
                lineA.push(' ');
                lineB.push(BasicColors.BLACK);
            }
            this.charBuffer.push(lineA);
            this.colorBuffer.push(lineB);
        }
    }

    renderBuffer() {
        var currentColor = this.colorBuffer[0][0];
        var currentElement = window.document.createElement("span");
        currentElement.setAttribute("style", "color: #" + currentColor.hex + "");
        this.outputElement.innerHTML = ''
        for (var i = 0; i < this.ySize; i++) {
            for (var j = 0; j < this.xSize; j++) {
                if (this.colorBuffer[i][j] == currentColor) {
                    currentElement.innerText += this.charBuffer[i][j];
                } else {
                    this.outputElement.appendChild(currentElement);
                    currentColor = this.colorBuffer[i][j];
                    currentElement = window.document.createElement("span");
                    currentElement.setAttribute("style", "color: #" + currentColor.hex + "");
                    currentElement.innerText += this.charBuffer[i][j];
                }
            }
            currentElement.innerText += '\n';
        }
        this.outputElement.appendChild(currentElement);
    }

    writeOnBuffer(text = '', color = BasicColors.BLACK) {
        for (var i = 0; i < text.length; i++) {
            this.writeCharOnBuffer(text.charAt(i), color);
        }
    }

    writeCharOnBuffer(char = ' ', color = BasicColors.BLACK) {
        switch (char) {
            case '\r':
                break;
            case '\n':
                this.posX = 0;
                this.moveCursorDown();
                break;
            case '\t':
                this.writeOnBuffer("    ");
                break;
            default:
                this.charBuffer[this.posY][this.posX] = char;
                this.colorBuffer[this.posY][this.posX] = color;
                this.moveCursorRight()
                break;
        }
    }

    setCursorPos(x = 0, y = 0) {
        if (x < this.xSize && x >= 0) {
            this.posX = x;
        }
        if (y < this.xSize && y >= 0) {
            this.posY = y;
        }
    }

    moveCursorUp() {
        if (this.posY > 0) {
            this.posY--;
        }
    }

    moveCursorDown() {
        if (this.posY + 1 < this.ySize) {
            this.posY++;
        } else {
            this.moveConsoleDown();
        }
    }

    moveCursorLeft() {
        if (this.posX > 0) {
            this.posX--;
        } else {
            this.posX = this.xSize - 1;
            this.moveCursorUp();
        }
    }

    moveCursorRight() {
        if (this.posX + 1 < this.xSize) {
            this.posX++;
        } else {
            this.posX = 0;
            this.moveCursorDown();
        }
    }

    moveConsoleDown() {
        this.charBuffer.shift();
        this.colorBuffer.shift();
        var lineA = [];
        var lineB = [];
        for (var j = 0; j < this.xSize; j++) {
            lineA.push(' ');
            lineB.push(BasicColors.BLACK);
        }
        this.charBuffer.push(lineA);
        this.colorBuffer.push(lineB);
    }

    clearConsole() {
        this.startBuffers();
    }
}