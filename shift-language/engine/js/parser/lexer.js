class Lexer {

    constructor() {
        this.tokenBuffer = "";
        this.tokenList = [];
        this.isReadingToken = false;
        this.tokenBufferStart = 0;
        this.pos = 0;
        this.currentSource = "";
        this.currentSourceLength = 0;
        this.sourceChars = [];
        this.checkAfterToken = false;
        this.afterTokenList = [];
        this.possibleAfterTokenError = null;
    }

    isWhiteSpace(char) {
        return char.trim().length == 0;
    }


    addToken(type, startPos, endPos, value) {
        if (this.checkAfterToken) {
            var foundToken = false;
            for (var token in this.afterTokenList) {
                if (this.afterTokenList[token] == type) {
                    foundToken = true;
                    this.checkAfterToken = false;
                    break;
                }
            }
            if (!foundToken) {
                return this.possibleAfterTokenError;
            }
        }
        this.isReadingToken = false;
        this.tokenList.push(new Token(type, startPos, endPos, value));
        return TOKEN_OK;
    }

    closeBuffer(startPos, endPos) {
        if (this.isReadingToken && startPos < endPos) {
            this.isReadingToken = false;
            for (var tokenVal in DynamicTokenList) {
                var token = DynamicTokenList[tokenVal];
                var value = token.lexerProcessor(this.tokenBuffer);
                if (value != null) {
                    return this.addToken(token.type, startPos, endPos, value);
                }
            }
            return new TokenError("No valid token found for \"" + this.currentSource.substring(startPos, endPos) + "\"", startPos, endPos);
        }
        return TOKEN_OK;
    }

    generateTokenListStr(tokenList) {
        var str = tokenList[0].name;
        var length = tokenList.length;
        for (var i = 1; i < length; i++) {
            str += ', ' + tokenList[i].name;
        }
        return str;
    }

    checkEndToken(startPos, endPos, type, beforePolicy, afterPolicy, beforePolicyTokenList, afterPolicyTokenList, value) {
        this.tokenBuffer = this.currentSource.substring(this.tokenBufferStart, startPos);

        var result = this.closeBuffer(this.tokenBufferStart, startPos);
        if (result != TOKEN_OK) {
            return result;
        }


        if (beforePolicy == TokenPolicy.HAS_TOKEN) {
            if (this.tokenList.length == 0 || this.isWhiteSpace(this.currentSource.charAt(startPos - 1))) {
                return new TokenError("Token \"" + type.name + "\" requires a token of the type(s) [" + this.generateTokenListStr(beforePolicyTokenList) + "] before it.", startPos, endPos);
            } else {
                var lastTokenType = this.tokenList[this.tokenList.length - 1].type;
                var foundToken = false;
                for (var token in beforePolicyTokenList) {
                    if (beforePolicyTokenList[token] == lastTokenType) {
                        foundToken = true;
                        break;
                    }
                }
                if (!foundToken) {
                    return new TokenError("Token \"" + type.name + "\" requires a token of the type(s) [" + this.generateTokenListStr(beforePolicyTokenList) + "] before it.", startPos, endPos);
                }
            }
        } else if (beforePolicy == TokenPolicy.HAS_TOKEN_OR_WHITESPACE || beforePolicy == TokenPolicy.HAS_TOKEN_OR_ADD_DUMMY_IF_WHITESPACE) {
            if (startPos != 0 && !this.isWhiteSpace(this.currentSource.charAt(startPos - 1))) {
                var lastTokenType = this.tokenList[this.tokenList.length - 1].type;
                var foundToken = false;
                for (var token in beforePolicyTokenList) {
                    if (beforePolicyTokenList[token] == lastTokenType) {
                        foundToken = true;
                        break;
                    }
                }
                if (!foundToken) {
                    return new TokenError("Token \"" + type.name + "\" requires a token of the type(s) [" + this.generateTokenListStr(beforePolicyTokenList) + "] or a Whitespace before it.", startPos, endPos);
                }

            } else if (beforePolicy == TokenPolicy.HAS_TOKEN_OR_ADD_DUMMY_IF_WHITESPACE) {
                result = this.addToken(TokenTypeList.DUMMY, startPos, startPos);
                if (result != TOKEN_OK) {
                    return result;
                }
            }
        }

        //The addition of the token is done here so it wont affect the detection of the next token

        result = this.addToken(type, startPos, endPos, value);
        if (result != TOKEN_OK) {
            return result;
        }

        if (afterPolicy == TokenPolicy.HAS_TOKEN) {
            if (this.pos == this.currentSourceLength || this.isWhiteSpace(this.currentSource.charAt(endPos))) {
                return new TokenError("Token \"" + type.name + "\" requires a token of the type(s) [" + this.generateTokenListStr(afterPolicyTokenList) + "] after it.", startPos, endPos);
            } else {
                this.checkAfterToken = true;
                this.afterTokenList = afterPolicyTokenList;
                this.possibleAfterTokenError = new TokenError("Token \"" + type.name + "\" requires a token of the type(s) [" + this.generateTokenListStr(afterPolicyTokenList) + "] after it.", startPos, endPos);
            }
        } else if (afterPolicy == TokenPolicy.HAS_TOKEN_OR_WHITESPACE) {
            if (this.pos < this.currentSourceLength && !this.isWhiteSpace(this.currentSource.charAt(endPos))) {
                this.checkAfterToken = true;
                this.afterTokenList = afterPolicyTokenList;
                this.possibleAfterTokenError = new TokenError("Token \"" + type.name + "\" requires a token of the type(s) [" + this.generateTokenListStr(afterPolicyTokenList) + "] or a Whitespace after it.", startPos, endPos);
            }
        }
        this.pos = endPos - 1;
        return TOKEN_OK;
    }

    generateStartPos(str, endPos) {
        return endPos - str.length
    }

    checkIfContainsToken() {
        var tokenName, token;
        var endPos = this.pos + 1;
        for (tokenName in SimpleTokenList) {
            token = SimpleTokenList[tokenName];
            if (this.tokenBuffer.endsWith(token.value)) {
                return this.checkEndToken(this.generateStartPos(token.value, endPos), endPos, token.type, token.beforePolicy, token.afterPolicy, token.beforePolicyTokenList, token.afterPolicyTokenList);
            }
        }
        for (tokenName in PairTokenList) {
            token = PairTokenList[tokenName];
            if (this.tokenBuffer.endsWith(token.startToken)) {
                return this.checkEndToken(this.generateStartPos(token.startToken, endPos), endPos, token.startTokenType, token.beforePolicy, undefined, token.beforePolicyTokenList, undefined);
            } else if (this.tokenBuffer.endsWith(token.endToken)) {
                return this.checkEndToken(this.generateStartPos(token.endToken, endPos), endPos, token.endTokenType, undefined, token.afterPolicy, undefined, token.afterPolicyTokenList);
            }
        }

        for (tokenName in ComplexTokenList) {
            token = ComplexTokenList[tokenName];
            if (this.tokenBuffer.endsWith(token.startToken)) {
                var startPos = this.pos - token.startToken.length + 1;
                /*var result = this.closeBuffer(this.tokenBufferStart,startPos);
                 if (result != TOKEN_OK) {
                 return result;
                 }*/
                this.isReadingToken = true;
                return token.lexerProcessor(this, startPos);
            }
        }

        return TOKEN_OK;
    }

    analyze(source = "") {
        this.currentSource = source;
        this.tokenList = [];
        this.isReadingToken = false;
        this.tokenBuffer = "";
        this.sourceChars = [...source];
        this.currentSourceLength = this.sourceChars.length;
        this.checkAfterToken = false;
        this.afterTokenList = [];
        this.possibleAfterTokenError = null;
        for (this.pos = 0; this.pos < this.currentSourceLength; this.pos++) {
            var currentChar = this.sourceChars[this.pos];
            if (!this.isWhiteSpace(currentChar)) {//Ignores whitespaces
                if (!this.isReadingToken) {
                    this.isReadingToken = true;
                    this.tokenBufferStart = this.pos;
                    this.tokenBuffer = "";
                }
                this.tokenBuffer += currentChar;
                var result = this.checkIfContainsToken();
                if (result != TOKEN_OK) {
                    return result;
                }
            } else if (this.isReadingToken) {//if it was previously reading a command
                var result = this.closeBuffer(this.tokenBufferStart, this.pos);
                if (result != TOKEN_OK) {
                    return result;
                }
            }
        }
        //Writes the last command
        var result = this.closeBuffer(this.tokenBufferStart, this.pos);
        if (result != TOKEN_OK) {
            return result;
        }
        return this.tokenList;
    }
}

