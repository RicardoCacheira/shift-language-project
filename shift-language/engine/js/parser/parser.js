/**
 * Created by Ricardo on 26/11/2016.
 */

/*
 1st Phase - Pair Token processing
 2nd Phase - Compound Token processing and ignore and error detection
 */
class PairBuffer {
    constructor(token, tokenStr, pos) {
        this.token = token;
        this.tokenStr = tokenStr;
        this.pos = pos;
    }
}

class FinalPhaseObj {
    constructor(tokenList) {
        this.tokenList = tokenList;
        this.finalList = [];
        this.pos = 0;
    }

    removeCurrentToken() {
        this.tokenList.splice(this.pos, 1);
        this.pos--;
    }
}

class Parser {
    constructor() {
        this.lexer = new Lexer();
    }

    parse(source) {
        this.sourceTokenList = this.lexer.analyze(source);
        if (this.sourceTokenList instanceof TokenError) {
            return this.sourceTokenList;
        }
        this.pos;
        this.pairTokenBuffer = [];
        for (this.pos = 0; this.pos < this.sourceTokenList.length; this.pos++) {
            var currentToken = this.sourceTokenList[this.pos];
            for (var tokenName in PairTokenList) {
                var pairToken = PairTokenList[tokenName];
                if (currentToken.type == pairToken.startTokenType) {
                    this.pairTokenBuffer.push(new PairBuffer(currentToken, pairToken.startToken, this.pos));
                    break;
                } else if (currentToken.type == pairToken.endTokenType) {
                    var lastToken = this.pairTokenBuffer.pop();
                    //No start tokens are in buffer when a end token is detected
                    if (lastToken == undefined) {
                        return new TokenError("No start token found for end token '" + pairToken.endToken + "'(" + pairToken.endTokenType.name + ").", currentToken.startPos, currentToken.endPos);
                    } else if (lastToken.token.type == pairToken.startTokenType) {//The token on the top of the pair buffer is the correct pair
                        var innerTokenList = this.sourceTokenList.slice(lastToken.pos, this.pos + 1);//adds the tokens inside the pair and the pair to a list
                        var tokensToRemoveNumber = innerTokenList.length;//The number of tokens to de removed(the number of tokens and the corresponding pair tokens)
                        this.sourceTokenList.splice(lastToken.pos, tokensToRemoveNumber);//removes the pair token and the tokens inside
                        this.pos = lastToken.pos - 1;//Sets the position to the position of the start pair token
                        var result = pairToken.pairTokenProcessor(innerTokenList, innerTokenList.shift().startPos, innerTokenList.pop().endPos, this);
                        if (result instanceof TokenError) {
                            return result;
                        }
                        //Puts the new Pair Token in the position of it's start token
                        this.pos = lastToken.pos;
                        this.sourceTokenList.splice(this.pos, 0, result);
                        break;
                    } else {
                        return new TokenError("Mismatch Pair Tokens '" + lastToken.tokenStr + "' (" + lastToken.token.type.name + ") and '" + pairToken.endToken + "' (" + pairToken.endTokenType.name + ").", lastToken.token.startPos, currentToken.endPos);
                    }
                }
            }
        }
        return this.nPhaseParse(0, this.sourceTokenList);

    }

    nPhaseParse(phase, tokenList) {
        if (phase == MAX_TOKEN_PARSE_PHASE) {
            var obj = new FinalPhaseObj(tokenList);
            for (; obj.pos < tokenList.length; obj.pos++) {
                var type = obj.tokenList[obj.pos].type;
                if (type.finalPhaseProcessor != undefined) {
                    var result = type.finalPhaseProcessor(obj);
                    if (result instanceof TokenError) {
                        return result;
                    }
                }

            }
            obj.finalList.push(obj.tokenList);
            return obj.finalList;
        }
        var resultList = [];
        var phaseCompoundList = CompoundTokenList[phase];
        var foundToken;
        while (tokenList.length > 0) {
            foundToken = false;
            for (var tokenName in phaseCompoundList) {
                var compoundToken = phaseCompoundList[tokenName];
                var result = this.checkCompoundToken(compoundToken, tokenList);
                if (result instanceof TokenError) {
                    return result;
                } else if (result != NO_COMPOUND_TOKEN_FOUND) {
                    resultList.push(result);
                    foundToken = true;
                    break;
                }
            }
            if (!foundToken) {
                resultList.push(tokenList.shift());
            }
        }

        //process tokens
        return this.nPhaseParse(phase + 1, resultList);
    }

    checkCompoundToken(compoundToken, tokenList) {
        var definitionList = compoundToken.definition;
        var definitionListPos = 0, tokenListPos = 0, definitionListLength = definitionList.length, tokenListLength = tokenList.length;
        var currentDefinition;
        var currentToken;
        while (definitionListPos < definitionListLength) {
            currentDefinition = definitionList[definitionListPos];
            currentToken = tokenList[tokenListPos];
            if (currentDefinition.multi) {
                if (tokenListPos < tokenListLength && currentToken.type === currentDefinition.type) {
                    tokenListPos++;
                } else {
                    definitionListPos++;
                }
            } else {
                if (tokenListPos < tokenListLength && currentToken.type === currentDefinition.type) {
                    tokenListPos++;
                    definitionListPos++;
                } else {
                    return NO_COMPOUND_TOKEN_FOUND;
                }
            }
        }
        if (definitionListPos == definitionListLength) {//All tokens in the definition were found
            var resultList = tokenList.slice(0, tokenListPos);
            tokenList.splice(0, tokenListPos);
            return compoundToken.nPhaseProcessor(resultList);
        } else {
            return NO_COMPOUND_TOKEN_FOUND;
        }
    }
}
var NO_COMPOUND_TOKEN_FOUND = 0;