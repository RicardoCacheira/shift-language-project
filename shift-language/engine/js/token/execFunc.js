//The exec function is the function that is called by the engine when the token is being processed

function variableExec(token, engine) {
    return engine.processVariable(this.generatePropertyList(token, engine), token.isLocal);
}

function setCommandExec(token, engine) {
    var obj = engine.popFromStack();
    if (obj instanceof Command) {
        return engine.setVariable(token.value, new UserCommand(obj));
    }
    if (obj instanceof RuntimeError) {
        return obj;
    }
    return new RuntimeError("The object must be of the Command type.");
}

function stringExec(token, engine) {
    engine.pushToStack(token.value);
    return RUNTIME_OK;
}

function dynamicStringExec(token, engine) {

}

function numberExec(token, engine) {
    engine.pushToStack(+token.value);
    return RUNTIME_OK;
}

function newArrayExec(token, engine) {
    var stack = engine.getStack();
    var aux = stack.length;
    var result = engine.runCommandList(token.value)
    if (result instanceof RuntimeError) {
        return result;
    }
    //The aux is the difference of length of the stack after execution;
    aux = stack.length - aux;
    if (aux > 0) {
        aux = engine.popMultipleFromStack(aux);
        if (aux instanceof RuntimeError) {
            return aux;
        }
        engine.pushToStack(aux);
    } else {
        engine.pushToStack([]);
    }
    return RUNTIME_OK;
}

function propertyExec(token, engine) {
    if (typeof token.value == "string") {
        return token.value;
    }
    //For performance reasons if the position is only one token the processing in the engine is bypassed
    if (token.value.length == 1 && token.value[0].length == 1) {
        var pos = token.value[0][0];
        //The token has to be a string or a number token
        if (pos.type == TokenTypeList.STRING || pos.type == TokenTypeList.NUMBER) {
            return pos.value;
        }
    }
    //TODO Test this when an input command is implemented
    //Commands that request a input will fail if inside a pos;
    if (engine.runCommandList(token.value) != RUNTIME_OK) {
        return new RuntimeError("Position commands cannot interrupt the execution.");
    }
    var stackValue = engine.popFromStack();
    if (stackValue instanceof RuntimeError) {
        return new RuntimeError("Empty stack found at the end of the property command execution.");
    }
    if ((typeof stackValue == "string") || (typeof stackValue == "number")) {
        return stackValue;
    }
    return new RuntimeError("Property must be a string or a number");
}

function commandExec(token, engine) {
    engine.pushToStack(new Command(token.value));
    return RUNTIME_OK;
}

function addVarExec(token, engine) {
    var variable = token.value;
    if (variable.isLastPropertyEmpty) {
        return engine.setVariable(variable, []);
    }
    return engine.setVariable(variable, {});
}

function setVarExec(token, engine) {
    var obj = engine.popFromStack();
    if (obj instanceof RuntimeError) {
        return obj;
    }
    return engine.setVariable(token.value, obj);
}

function remVarExec(token, engine) {
    var variable = token.value;
    if (variable.isLastPropertyEmpty) {
        var propertyList = TokenTypeList.VARIABLE.generatePropertyList(variable, engine);
        variable = engine.getVariableFromEngine(propertyList, variable.isLocal);
        if (variable instanceof RuntimeError) {
            return variable
        }
        variable.splice(0, variable.length);
        return RUNTIME_OK;
    }

    return engine.setVariable(variable, undefined);
}

//TODO Improve code
function pushValIntoExec(token, engine) {
    var obj = engine.popFromStack();
    if (obj instanceof RuntimeError) {
        return obj;
    }
    var variable = token.value;
    var propertyList = TokenTypeList.VARIABLE.generatePropertyList(variable, engine);
    if (variable.isLastPropertyEmpty) {
        variable = engine.getVariableFromEngine(propertyList, variable.isLocal);
        if (variable === undefined || variable === null) {
            return new RuntimeError("You can't push into a undefined/null value.");
        }
        if (variable instanceof RuntimeError) {
            return variable
        }
        if (variable instanceof Array) {
            variable.push(obj);
            return RUNTIME_OK;
        }
        return new RuntimeError("Pushing is only allowed in Arrays.");
    }
    var desiredPos = +(propertyList.pop());
    if (Number.isInteger(desiredPos) && desiredPos >= 0) {
        variable = engine.getVariableFromEngine(propertyList, variable.isLocal);
        if (variable === undefined || variable === null) {
            return new RuntimeError("You can't push into a undefined/null value.");
        }
        if (variable instanceof RuntimeError) {
            return variable
        }
        if (variable instanceof Array) {
            variable.splice(desiredPos, 0, obj);
            return RUNTIME_OK;
        }
        return new RuntimeError("Pushing is only allowed in Arrays.");
    }
    return new RuntimeError("The pop from must have an valid position at the end.");
}

//TODO Reduce duplicated code from push val
function popValFromExec(token, engine) {
    var variable = token.value;
    var propertyList = TokenTypeList.VARIABLE.generatePropertyList(variable, engine);
    if (variable.isLastPropertyEmpty) {
        variable = engine.getVariableFromEngine(propertyList, variable.isLocal);
        if (variable === undefined || variable === null) {
            return new RuntimeError("You can't pop from a undefined/null value.");
        }
        if (variable instanceof RuntimeError) {
            return variable
        }
        if (variable instanceof Array) {
            if (variable === engine.getStack()) {
                var result = engine.popFromStack();
                if (result instanceof RuntimeError) {
                    return result;
                }
                return RUNTIME_OK;
            }
            if (variable.length > 0) {
                if (variable.dropOnPop) {
                    variable.pop();
                } else {
                    engine.pushToStack(variable.pop());
                }
                return RUNTIME_OK;
            }
            return new RuntimeError("Unable to pop an empty Array.");
        }
        return new RuntimeError("Popping is only allowed in Arrays.");
    }

    var desiredPos = +(propertyList.pop());
    if (Number.isInteger(desiredPos) && desiredPos >= 0) {
        variable = engine.getVariableFromEngine(propertyList, variable.isLocal);
        if (variable === undefined || variable === null) {
            return new RuntimeError("You can't pop from a undefined/null value.");
        }
        if (variable instanceof RuntimeError) {
            return variable
        }
        if (variable instanceof Array) {
            if (variable === engine.getStack()) {
                return new RuntimeError("You can only pop the top of the stack.");
            }
            if (variable.dropOnPop) {
                variable.splice(desiredPos, 1)[0]
            } else {
                engine.pushToStack(variable.splice(desiredPos, 1)[0]);
            }
            return RUNTIME_OK;

        }
        return new RuntimeError("Popping is only allowed in Arrays.");
    }
    return new RuntimeError("The pop from must have an valid position at the end.");
}

function popMultiExec(token, engine) {
    var result = engine.runCommandList(token.value)
    if (result instanceof RuntimeError) {
        return result;
    }
    var aux = engine.popFromStack();
    if (aux instanceof RuntimeError) {
        return aux;
    }
    if (Number.isInteger(aux) && aux >= 0) {
        aux = engine.popMultipleFromStack(aux);
        if (aux instanceof RuntimeError) {
            return aux;
        }
        engine.pushToStack(aux);
    } else {
        return new RuntimeError("The number of variables to pop must be a non negative integer.");
    }
    return RUNTIME_OK;
}

function trueExec(token, engine) {
    engine.pushToStack(true);
    return RUNTIME_OK;
}

function falseExec(token, engine) {
    engine.pushToStack(false);
    return RUNTIME_OK;
}

function callWithArgsExec(token, engine) {
    engine.lockStack();
    var result = engine.runCommandList(token.args);
    if (result instanceof RuntimeError) {
        engine.unlockStack();
        return result;
    }
    result = TokenTypeList.VARIABLE.getVariableFromEngine(token.value, engine);
    if (result instanceof RuntimeError) {
        engine.unlockStack();
        return result;
    }
    if (result instanceof UserCommand) {
        result = engine.runCommandSeparate(result);
        engine.startExecution();//the command can stop execution if an return of break are used
    } else if (result instanceof ModuleCommand) {
        result = engine.runModuleCommand(result);
    } else {
        result = new RuntimeError("The chosen variable must be a command.");
    }
    engine.unlockStack();
    engine.pushReturnValueIfNew();//Puts the result in the parent command stack
    return result;
}

function returnExec(token, engine) {
    var returnValue = engine.popFromStack();
    if (returnValue instanceof RuntimeError) {
        return returnValue;
    }
    engine.setReturnValue(returnValue);
    engine.stopExecution();
    return RUNTIME_OK;
}

function breakExec(token, engine) {
    engine.stopExecution();
    return RUNTIME_OK;
}