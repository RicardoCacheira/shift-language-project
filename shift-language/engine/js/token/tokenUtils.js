/*--------------------------------------------------Token Utilities---------------------------------------------------*/

const TOKEN_OK = 0;

class Token {
    constructor(type, startPos, endPos, value) {
        this.type = type;
        this.startPos = startPos;
        this.endPos = endPos;
        if (value != undefined) {
            this.value = value;
        }
    }

    exec(engine) {
        return this.type.exec(this, engine);
    }
}


var TokenPolicy = {
    HAS_TOKEN: 0,
    HAS_TOKEN_OR_WHITESPACE: 1,
    HAS_TOKEN_OR_ADD_DUMMY_IF_WHITESPACE: 2//Will only work in the beforePolicy
}

//token[*];token[*]...
//* -> there can be many of this token
function definitionListGenerator(definition) {
    var aux = definition.split(";");
    var result = [];
    var length = aux.length;
    for (var i = 0; i < length; i++) {
        var val = aux[i];
        if (val.charAt(0) == '*') {
            result.push({type: TokenTypeList[val.substring(1)], multi: true});
        } else {
            result.push({type: TokenTypeList[val], multi: false});
        }
    }
    return result;
}

/*--------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------Variable Token Utilities----------------------------------------------*/

//Returns the state of the property list given
function checkPropertyState(propertyList) {
    var length = propertyList.length;
    for (var i = 0; i < length - 1; i++) {
        if (TokenTypeList.PROPERTY.isEmptyProperty(propertyList[i])) {
            return this.INVALID_EMPTY;
        }
    }
    if (length > 0 && TokenTypeList.PROPERTY.isEmptyProperty(propertyList[length - 1])) {
        return this.LAST_EMPTY;
    }
    return this.NO_EMPTY;
}

function generatePropertyList(token, engine) {
    var propertyList = [token.value];
    var length = token.posList.length
    var aux;
    for (var i = 0; i < length; i++) {
        var posToken = token.posList[i];
        aux = posToken.type.exec(posToken, engine);
        if (aux instanceof RuntimeError) {
            return aux;
        }
        propertyList.push(aux);
    }
    return propertyList
}

function getVariableFromEngine(token, engine) {
    return engine.getVariable(this.generatePropertyList(token, engine), token.isLocal);
}

/*------------------------------------------------------------------------------------------------------*/

/*--------------------------------------Property Token Utilities-----------------------------------------*/

function isEmptyProperty(token) {
    return (!(typeof token.value == "string") && token.value.length == 1 && token.value[0].length == 0);
}

/*------------------------------------------------------------------------------------------------------*/