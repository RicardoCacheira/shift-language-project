/*-------------------------------------------Dynamic Token Lexer Processors-------------------------------------------*/

function numberLexerProcessor(buffer) {
    if (isNaN(buffer)) {
        return null;
    }
    return buffer;
}

function idLexerProcessor(buffer) {
    return buffer;
}

/*--------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------Complex Token Lexer Processors-------------------------------------------*/

function stringLexerProcessor(lexer, startPos) {
    var strBuffer = [];
    lexer.pos++;
    if (lexer.pos == lexer.currentSourceLength) {
        return new TokenError("Open String definition.", startPos, lexer.pos)
    }
    while (lexer.sourceChars[lexer.pos] != this.startToken) {
        if (lexer.sourceChars[lexer.pos] == '\\') {
            lexer.pos++;
            if (lexer.pos == lexer.currentSourceLength) {
                return new TokenError("Line end with '\\'.", lexer.pos - 1, lexer.pos)
            }
            switch (lexer.sourceChars[lexer.pos]) {
                case '\n':
                    return new TokenError("Open String definition.", startPos, lexer.pos);
                case 'n':
                    strBuffer.push('\n');
                    break;
                case 't':
                    strBuffer.push('\t');
                    break;
                case 'r':
                    strBuffer.push('\r');
                    break;
                case 'b':
                    strBuffer.push('\b');
                    break;
                case this.startToken:
                    strBuffer.push(this.startToken);
                    break;
                case '\\':
                    strBuffer.push('\\');
                    break;
                default:
                    return new TokenError("Invalid escape sequence (\\" + lexer.sourceChars[lexer.pos] + ").", lexer.pos - 1, lexer.pos + 1)
            }
        } else {
            strBuffer.push(lexer.sourceChars[lexer.pos]);
        }
        lexer.pos++;
        if (lexer.pos == lexer.currentSourceLength) {
            return new TokenError("Open String definition.", startPos, lexer.pos);
        }
    }
    //TODO Analise this line
    return lexer.checkEndToken(startPos, lexer.pos + 1, this.type, this.beforePolicy, this.afterPolicy, this.beforePolicyTokenList, this.afterPolicyTokenList, strBuffer.join(""));
}

function dynamicStringLexerProcessor(lexer, startPos) {
    //TODO Make the processor
}

function commentLexerProcessor(lexer, startPos) {
    lexer.pos++;
    if (lexer.pos != lexer.currentSourceLength) {
        while (lexer.sourceChars[lexer.pos] != '\n') {
            lexer.pos++;
            if (lexer.pos == lexer.currentSourceLength) {
                break;
            }
        }
    }
    return lexer.checkEndToken(startPos, lexer.pos, this.type);
}

/*--------------------------------------------------------------------------------------------------------------------*/