var TokenTypeList = {
    VARIABLE: {
        name: "VARIABLE",
        NO_EMPTY: 0,
        LAST_EMPTY: 1,
        INVALID_EMPTY: 2,
        checkPropertyState: checkPropertyState,
        generatePropertyList: generatePropertyList,
        finalPhaseProcessor: variableFinalPhaseProcessor,
        getVariableFromEngine: getVariableFromEngine,
        exec: variableExec
    },
    SET_COMMAND: {
        name: "SET_COMMAND",
        exec: setCommandExec
    },
    STRING: {
        name: "STRING",
        exec: stringExec
    },
    DYNAMIC_STRING: {
        name: "DYNAMIC_STRING",
        exec: dynamicStringExec
    },
    NUMBER: {
        name: "NUMBER",
        exec: numberExec
    },
    COMMENT: {
        name: "COMMENT",
        finalPhaseProcessor: commentFinalPhaseProcessor
    },
    BRACKETS_START: {
        name: "BRACKETS_START",
    },
    BRACKETS_END: {
        name: "BRACKETS_END",
    },
    BRACKETS: {
        name: "BRACKETS",
    },
    NEW_ARRAY: {
        name: "NEW_ARRAY",
        exec: newArrayExec
    },
    PROPERTY: {
        name: "PROPERTY",
        isEmptyProperty: isEmptyProperty,
        exec: propertyExec
    },
    COMMAND_OBJECT_START: {
        name: "COMMAND_OBJECT_START",

    },
    COMMAND_OBJECT_END: {
        name: "COMMAND_OBJECT_END",
    },
    COMMAND_OBJECT: {
        name: "COMMAND_OBJECT",
        exec: commandExec
    },
    ARG_BUILDER_START: {
        name: "ARG_BUILDER_START",
    },
    ARG_BUILDER_END: {
        name: "ARG_BUILDER_END",
    },
    ARG_BUILDER: {
        name: "ARG_BUILDER",
    },
    COMMAND_END: {
        name: "COMMAND_END",
        finalPhaseProcessor: commandEndFinalPhaseProcessor
    },
    LOCAL_VAR: {
        name: "LOCAL_VAR"
    },
    ADD_VAR: {
        name: "ADD_VAR",
        exec: addVarExec
    },
    SET_VAR: {
        name: "SET_VAR",
        exec: setVarExec
    },
    REM_VAR: {
        name: "REM_VAR",
        exec: remVarExec
    },
    COMMA: {
        name: "COMMA",
        finalPhaseProcessor: commaFinalPhaseProcessor
    },
    DOT: {
        name: "DOT"
    },
    PUSH_VAL_INTO: {
        name: "PUSH_VAL_INTO",
        exec: pushValIntoExec
    },
    POP_VAL_FROM: {
        name: "POP_VAL_FROM",
        exec: popValFromExec
    },
    POP_MULTI: {
        name: "POP_MULTI",
        exec: popMultiExec
    },
    TRUE: {
        name: "TRUE",
        exec: trueExec
    },
    FALSE: {
        name: "FALSE",
        exec: falseExec
    },
    DUMMY: {
        name: "DUMMY",
        finalPhaseProcessor: dummyFinalPhaseProcessor
    },
    CALL_WITH_ARGS: {
        name: "CALL_WITH_ARGS",
        exec: callWithArgsExec
    },
    RETURN: {
        name: "RETURN",
        exec: returnExec
    },
    BREAK: {
        name: "BREAK",
        exec: breakExec
    }

}

//The order defines the priority of the tokens
//The token processor returns null if the buffer is invalid for the type
//The buffer is of the type string.
var DynamicTokenList = {
    NUMBER: {
        type: TokenTypeList.NUMBER,
        lexerProcessor: numberLexerProcessor
    },
    ID: {
        type: TokenTypeList.VARIABLE,
        lexerProcessor: idLexerProcessor
    }
}

var ComplexTokenList = {
    STRING: {
        startToken: '"',
        type: TokenTypeList.STRING,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.BRACKETS_START, TokenTypeList.COMMAND_OBJECT_START, TokenTypeList.ARG_BUILDER_START],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.BRACKETS_END, TokenTypeList.COMMAND_OBJECT_END, TokenTypeList.ARG_BUILDER_END],
        lexerProcessor: stringLexerProcessor
    },
    DYNAMIC_STRING: {
        startToken: "'",
        type: TokenTypeList.DYNAMIC_STRING,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END],
        lexerProcessor: dynamicStringLexerProcessor
    },
    COMMENT: {
        startToken: "//",
        type: TokenTypeList.COMMENT,
        lexerProcessor: commentLexerProcessor
    }
}

var PairTokenList = {
    COMMAND_OBJECT: {
        startToken: "{",
        startTokenType: TokenTypeList.COMMAND_OBJECT_START,
        endToken: "}",
        endTokenType: TokenTypeList.COMMAND_OBJECT_END,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.BRACKETS_START, TokenTypeList.COMMAND_OBJECT_START, TokenTypeList.ARG_BUILDER_START],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.BRACKETS_END, TokenTypeList.COMMAND_OBJECT_END, TokenTypeList.ARG_BUILDER_END],
        type: TokenTypeList.COMMAND_OBJECT,
        pairTokenProcessor: genericPairTokenProcessor
    },
    BRACKETS: {
        startToken: "[",
        startTokenType: TokenTypeList.BRACKETS_START,
        endToken: "]",
        endTokenType: TokenTypeList.BRACKETS_END,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_ADD_DUMMY_IF_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.BRACKETS_END, TokenTypeList.BRACKETS_START, TokenTypeList.COMMAND_OBJECT_START, TokenTypeList.ARG_BUILDER_START, TokenTypeList.PUSH_VAL_INTO],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.BRACKETS_START, TokenTypeList.COMMAND_END, TokenTypeList.BRACKETS_END, TokenTypeList.COMMAND_OBJECT_END, TokenTypeList.ARG_BUILDER_END, TokenTypeList.DOT],
        type: TokenTypeList.BRACKETS,
        pairTokenProcessor: genericPairTokenProcessor
    },
    ARG_BUILDER: {//This exec will insert all the tokens that contains
        startToken: "(",
        startTokenType: TokenTypeList.ARG_BUILDER_START,
        endToken: ")",
        endTokenType: TokenTypeList.ARG_BUILDER_END,
        beforePolicy: TokenPolicy.HAS_TOKEN,
        beforePolicyTokenList: [TokenTypeList.BRACKETS_END, TokenTypeList.VARIABLE],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.BRACKETS_END, TokenTypeList.COMMAND_OBJECT_END, TokenTypeList.ARG_BUILDER_END],
        type: TokenTypeList.ARG_BUILDER,
        pairTokenProcessor: argBuilderPairTokenProcessor
    }
}

var SimpleTokenList = {
    COMMAND_END: {
        value: ";",
        type: TokenTypeList.COMMAND_END
    },
    LOCAL_VAR: {
        value: "$",
        type: TokenTypeList.LOCAL_VAR,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.ADD_VAR, TokenTypeList.SET_VAR, TokenTypeList.REM_VAR],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE]
    },
    COMMA: {
        value: ",",
        type: TokenTypeList.COMMA
    },
    TRUE: {
        value: "true",
        type: TokenTypeList.TRUE,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_END]
    },
    FALSE: {
        value: "false",
        type: TokenTypeList.FALSE,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_END]
    },
    RETURN: {
        value: "return",
        type: TokenTypeList.RETURN,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_END]
    },
    BREAK: {
        value: "break",
        type: TokenTypeList.BREAK,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END],
        afterPolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        afterPolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_END]
    },
    SET_COMMAND: {
        value: "=>",
        type: TokenTypeList.SET_COMMAND,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.LOCAL_VAR]
    },
    ADD_VAR: {
        value: "-+",
        type: TokenTypeList.ADD_VAR,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.LOCAL_VAR]
    },
    SET_VAR: {
        value: "->",
        type: TokenTypeList.SET_VAR,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.LOCAL_VAR]
    },
    REM_VAR: {
        value: "--",
        type: TokenTypeList.REM_VAR,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.LOCAL_VAR]
    },
    DOT: {
        value: ".",
        type: TokenTypeList.DOT,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.BRACKETS_END, TokenTypeList.VARIABLE, TokenTypeList.NUMBER],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.NUMBER]
    },
    PUSH_VAL_INTO: {
        value: ">>",
        type: TokenTypeList.PUSH_VAL_INTO,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.LOCAL_VAR, TokenTypeList.BRACKETS_START]
    },
    POP_VAL_FROM: {
        value: "<<",
        type: TokenTypeList.POP_VAL_FROM,
        beforePolicy: TokenPolicy.HAS_TOKEN_OR_WHITESPACE,
        beforePolicyTokenList: [TokenTypeList.COMMAND_END, TokenTypeList.COMMAND_OBJECT_START],
        afterPolicy: TokenPolicy.HAS_TOKEN,
        afterPolicyTokenList: [TokenTypeList.VARIABLE, TokenTypeList.LOCAL_VAR]
    }
}

var CompoundTokenList = [
    {
        SIMPLE_PROPERTY: {
            type: TokenTypeList.PROPERTY,
            definition: definitionListGenerator("DOT;VARIABLE"),
            nPhaseProcessor: simplePropertyNPhaseProcessor
        },
        DECIMAL_NUMBER: {
            type: TokenTypeList.NUMBER,
            definition: definitionListGenerator("NUMBER;DOT;NUMBER"),
            nPhaseProcessor: decimalNumberNPhaseProcessor
        },
        DECIMAL_NUMBER2: {
            type: TokenTypeList.NUMBER,
            definition: definitionListGenerator("DOT;NUMBER"),
            nPhaseProcessor: decimalNumber2NPhaseProcessor
        },
        POP_MULTI: {
            type: TokenTypeList.POP_MULTI,
            definition: definitionListGenerator("PUSH_VAL_INTO;BRACKETS"),
            nPhaseProcessor: popMultiNPhaseProcessor
        },
        NEW_ARRAY: {
            type: TokenTypeList.NEW_ARRAY,
            definition: definitionListGenerator("DUMMY;BRACKETS"),
            nPhaseProcessor: newArrayNPhaseProcessor
        }
    },
    {
        COMPLEX_PROPERTY: {
            type: TokenTypeList.PROPERTY,
            definition: definitionListGenerator("BRACKETS"),
            nPhaseProcessor: complexPropertyNPhaseProcessor
        }
    },
    {
        //Catches numbers that have more than one decimal point
        INVALID_DOT: {
            definition: definitionListGenerator("DOT"),
            nPhaseProcessor: invalidDotNPhaseProcessor
        },
        COMPOUND_VARIABLE: {
            type: TokenTypeList.VARIABLE,
            definition: definitionListGenerator("VARIABLE;*PROPERTY"),
            nPhaseProcessor: compoundVariableNPhaseProcessor
        }
    },
    {
        INVALID_PROPERTY: {
            definition: definitionListGenerator("PROPERTY"),
            nPhaseProcessor: invalidPropertyNPhaseProcessor
        },
        LOCAL_VAR: {
            type: TokenTypeList.VARIABLE,
            definition: definitionListGenerator("LOCAL_VAR;VARIABLE"),
            nPhaseProcessor: localVarNPhaseProcessor
        },
    },
    {
        CALLBACK_WITH_ARGS: {
            type: TokenTypeList.CALL_WITH_ARGS,
            definition: definitionListGenerator("VARIABLE;ARG_BUILDER"),
            nPhaseProcessor: callbackWithArgsNPhaseProcessor
        },
        SET_COMMAND: {
            type: TokenTypeList.SET_COMMAND,
            definition: definitionListGenerator("SET_COMMAND;VARIABLE"),
            nPhaseProcessor: setCommandNPhaseProcessor
        },
        ADD_VAR: {
            type: TokenTypeList.ADD_VAR,
            definition: definitionListGenerator("ADD_VAR;VARIABLE"),
            nPhaseProcessor: addVarNPhaseProcessor
        },
        SET_VAR: {
            type: TokenTypeList.SET_VAR,
            definition: definitionListGenerator("SET_VAR;VARIABLE"),
            nPhaseProcessor: setVarNPhaseProcessor

        },
        REM_VAR: {
            type: TokenTypeList.REM_VAR,
            definition: definitionListGenerator("REM_VAR;VARIABLE"),
            nPhaseProcessor: remVarNPhaseProcessor
        },
        PUSH_VAL_INTO: {
            type: TokenTypeList.PUSH_VAL_INTO,
            definition: definitionListGenerator("PUSH_VAL_INTO;VARIABLE"),
            nPhaseProcessor: pushValIntoNPhaseProcessor
        },
        POP_VAL_FROM: {
            type: TokenTypeList.POP_VAL_FROM,
            definition: definitionListGenerator("POP_VAL_FROM;VARIABLE"),
            nPhaseProcessor: popValFromNPhaseProcessor
        }
    },
    {
        INVALID_ARG_BUILDER: {
            definition: definitionListGenerator("ARG_BUILDER"),
            nPhaseProcessor: invalidArgBuilderNPhaseProcessor
        },
    }
]

var MAX_TOKEN_PARSE_PHASE = CompoundTokenList.length;



