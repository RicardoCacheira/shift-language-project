/*-----------------------------------------------Pair Phase Processors------------------------------------------------*/

function genericPairTokenProcessor(innerTokenList, startPos, endPos, parser) {
    var result = parser.nPhaseParse(0, innerTokenList);
    if (result instanceof TokenError) {//TODO Revise the check of the Errors(aka Maybe remove the use of TOKEN_OK)
        return result;
    }
    return new Token(this.type, startPos, endPos, result);
}

//TODO Remove duplicate code
function argBuilderPairTokenProcessor(innerTokenList, startPos, endPos, parser) {
    var length = innerTokenList.length
    var result;
    for (var i = 0; i < length; i++) {
        var token = innerTokenList[i];
        if (token.type == TokenTypeList.COMMAND_END) {
            return new TokenError("The argument builder must not contain Command End tokens.", token.startPos, token.endPos);
        }
    }
    result = parser.nPhaseParse(0, innerTokenList);
    if (result instanceof TokenError) {
        return result;
    }
    return new Token(this.type, startPos, endPos, result);
}

/*--------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------N Phase Processors-------------------------------------------------*/

function simplePropertyNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[1].value);
}


function decimalNumberNPhaseProcessor(tokenList) {
    var num = tokenList[0].value + '.' + tokenList[2].value;
    if (isNaN(num)) {
        return new TokenError("Invalid decimal Number '" + num + "'.", tokenList[0].startPos, tokenList[2].endPos);
    }
    return new Token(this.type, tokenList[0].startPos, tokenList[2].endPos, num);
}

function decimalNumber2NPhaseProcessor(tokenList) {
    var num = '0.' + tokenList[1].value;
    if (isNaN(num)) {
        return new TokenError("Invalid decimal Number '" + num + "'.", tokenList[0].startPos, tokenList[1].endPos);
    }
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, num);
}

function popMultiNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[1].startPos, tokenList[1].endPos, tokenList[1].value);

}

function newArrayNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[1].startPos, tokenList[1].endPos, tokenList[1].value);

}

function complexPropertyNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[0].startPos, tokenList[0].endPos, tokenList[0].value);

}

function invalidDotNPhaseProcessor(tokenList) {
    return new TokenError("Invalid Dot.", tokenList[0].startPos, tokenList[0].endPos);

}

function compoundVariableNPhaseProcessor(tokenList) {
    var firstToken = tokenList[0];
    var token = new Token(this.type, firstToken.startPos, tokenList[tokenList.length - 1].endPos, firstToken.value);
    tokenList.shift();
    token.posList = tokenList;
    switch (this.type.checkPropertyState(tokenList)) {
        case this.type.INVALID_EMPTY:
            return new RuntimeError("A compound property token cannot be empty.");
        case this.type.LAST_EMPTY:
            token.isLastPropertyEmpty = true;
            token.posList.pop();
            break;
        default:
            token.isLastPropertyEmpty = false;
    }
    token.isLocal = false;
    return token;
}

function invalidPropertyNPhaseProcessor(tokenList) {
    return new TokenError("Invalid Property.", tokenList[0].startPos, tokenList[0].endPos);
}

function localVarNPhaseProcessor(tokenList) {
    tokenList[1].isLocal = true;
    return tokenList[1];
}

function callbackWithArgsNPhaseProcessor(tokenList) {
    var token = new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[0]);
    token.args = tokenList[1].value;
    return token;
}

function setCommandNPhaseProcessor(tokenList) {
    if (tokenList[1].isLastPropertyEmpty) {
        return new TokenError("A compound property token cannot be empty.", tokenList[0].startPos, tokenList[1].endPos);
    }
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[1]);
}

function addVarNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[1]);

}

function setVarNPhaseProcessor(tokenList) {
    if (tokenList[1].isLastPropertyEmpty) {
        return new TokenError("A compound property token cannot be empty.", tokenList[0].startPos, tokenList[1].endPos);
    }
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[1]);
}

function remVarNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[1]);

}

function pushValIntoNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[1]);

}

function popValFromNPhaseProcessor(tokenList) {
    return new Token(this.type, tokenList[0].startPos, tokenList[1].endPos, tokenList[1]);

}

function invalidArgBuilderNPhaseProcessor(tokenList) {
    return new TokenError("An Argument builder must be after a variable.", tokenList[0].startPos, tokenList[0].endPos);

}

/*--------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------Final Phase Processors-----------------------------------------------*/

function variableFinalPhaseProcessor(finalPhaseObj) {
    if (finalPhaseObj.tokenList[finalPhaseObj.pos].isLastPropertyEmpty) {
        return new TokenError("A compound property token cannot be empty.", finalPhaseObj.tokenList[finalPhaseObj.pos].startPos, finalPhaseObj.tokenList[finalPhaseObj.pos].endPos);
    }
    return TOKEN_OK;
}

function commentFinalPhaseProcessor(finalPhaseObj) {
    finalPhaseObj.removeCurrentToken();
    return TOKEN_OK;
}

function commandEndFinalPhaseProcessor(finalPhaseObj) {
    finalPhaseObj.finalList.push(finalPhaseObj.tokenList.splice(0, finalPhaseObj.pos));
    finalPhaseObj.tokenList.shift();
    finalPhaseObj.pos = 0;
}

function commaFinalPhaseProcessor(finalPhaseObj) {
    finalPhaseObj.removeCurrentToken();
    return TOKEN_OK;
}

function dummyFinalPhaseProcessor(finalPhaseObj) {
    finalPhaseObj.removeCurrentToken();
    return TOKEN_OK;
}

/*--------------------------------------------------------------------------------------------------------------------*/