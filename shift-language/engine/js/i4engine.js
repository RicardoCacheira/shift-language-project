const RUNTIME_OK = 0;

var StatusList = {
    STOP: 0,
    WAITING_COMMAND: 1,
    RUNNING_COMMAND: 2,
    INPUT: 3
}

var OutputType = {
    PLAIN: 0,
    INFO: 1,
    ERROR: 2,
    CUSTOM: 3,
}

var simpleOutput = {
    processOutput: function (output) {
        console.log(output);
    }
}

class I4Engine {
    constructor() {
        this.isRunning = false;
        this.parser = new Parser();
        this.status = StatusList.WAITING_COMMAND;
        this.globalVarList = {};
        this.localVarList = [];
        this.stack = [];
        this.stackLockList = [];
        this.currentStackLock = 0;
        this.outputList = [];
        this.outputNum = 0;
        this.currentLocalMemory = -1;
        this.returnValue = undefined;
        this.isReturnValueNew = false;
        this.theme = {
            PLAIN: BasicColors.BLACK,
            INFO: BasicColors.BLUE,
            ERROR: BasicColors.RED
        }
    }

    loadGlobalModules() {
        var engine = this;
        moduleList.forEach(function (module) {
            module.loadModule(engine);
        });
    }

    addModuleCommand(command) {
        this.globalVarList[command.id] = command;
    }

    pushToStack(value) {
        this.getStack().push(value);
    }

    pushMultipleToStack(values) {
        this.getStack().push(...values);
    }

    popFromStack() {
        if (this.getStackLength() == 0) {
            return new RuntimeError("Pop on empty Stack.");
        }
        return this.getStack().pop();//Removes the argument separator
    }

    popMultipleFromStack(number) {
        if (this.getStackLength() < number) {
            return new RuntimeError("Not enough objects to pop in the Stack.");
        }
        return this.getStack().splice(this.getStack().length - number, number);
    }

    popAllFromStack() {
        return this.getStack().splice(this.currentStackLock, this.getStackLength());
    }


    /**
     * Returns 0 if all ok, else RuntimeError
     */
    runCommandList(tokenList) {
        var commandNum = tokenList.length
        var execResult;
        for (var i = 0; i < commandNum && this.isRunning; i++) {
            var tokenNum = tokenList[i].length
            for (var j = 0; j < tokenNum && this.isRunning; j++) {
                execResult = tokenList[i][j].type.exec(tokenList[i][j], this);
                if (execResult instanceof RuntimeError) {
                    return execResult;
                }
            }
        }
        return RUNTIME_OK;
    }

    runCommandObject(obj) {
        return obj.run(this);
    }

    //This will have its own local variable list, separate from the command that's currently running
    runCommandSeparate(obj) {
        return this.runCommandObject(obj);
    }

    //Breaks all current executions
    stopExecution() {
        this.isRunning = false;
    }

    startExecution() {
        this.isRunning = true;
    }

    input(input) {
        if (this.status == StatusList.WAITING_COMMAND) {
            this.status = StatusList.RUNNING_COMMAND;
            var parseResult = this.parser.parse(input);
            if (parseResult instanceof TokenError) {
                this.showError(parseResult, input);
            } else {
                this.startExecution();
                var runResult = this.runCommandList(parseResult);
                if (runResult instanceof RuntimeError) {
                    this.showError(runResult.msg);
                }
                this.stopExecution();
            }
            this.status = StatusList.WAITING_COMMAND;
        }

    }

    addInputSystem(inputSystem) {
        this.inputSystem = inputSystem;
    }

    requestInput() {
        if (this.inputSystem != undefined) {
            return this.inputSystem.requestInput();
        }
        return new RuntimeError("No input system found.");
    }

    plainOutput(output) {
        this.output(output, OutputType.PLAIN);
    }

    showInfo(info) {
        this.output(info, OutputType.INFO);
    }

    showError(error, src) {
        if (error instanceof TokenError) {
            this.output("\n" + error.msg + "\n\n", OutputType.ERROR);
            this.output(src.substring(0, error.startPos), OutputType.INFO);
            this.output(src.substring(error.startPos, error.endPos), OutputType.ERROR);
            this.output(src.substring(error.endPos) + "\n", OutputType.INFO);
        } else {
            this.output("\n" + error + "\n", OutputType.ERROR);
        }

    }

    customOutput(output, custom) {
        this.output(output, OutputType.CUSTOM, custom);
    }

    output(output, type = OutputType.PLAIN, custom) {
        output = "" + output;
        for (var currentOutput = 0; currentOutput < this.outputNum; currentOutput++) {
            this.outputList[currentOutput].processOutput(output, type, custom);
        }
    }

    addOutput(output) {
        this.outputList.push(output);
        this.outputNum++;
    }

//TODO make it work with local
    processVariable(propertyList, isLocal) {
        var desiredVariable;
        var length = propertyList.length;
        if (isLocal) {
            desiredVariable = this.localVarList[this.currentLocalMemory][propertyList[0]];
        } else {
            //checks for special tokens
            switch (propertyList[0]) {
                case "stack":
                    if (length == 1) {
                        return new RuntimeError("Stack cannot be added to itself.");
                    }
                    desiredVariable = this.getStack();
                    break;
                default:
                    desiredVariable = this.globalVarList[propertyList[0]];
            }
        }
        for (var i = 1; i < length; i++) {
            if (desiredVariable === undefined || desiredVariable === null) {
                //TODO Improve the error message to include the variable that caused the error
                return new RuntimeError("Undefined/null objects don't contain any properties");
            }
            desiredVariable = desiredVariable[propertyList[i]];
        }
        if (desiredVariable instanceof UserCommand) {
            var result = this.runCommandSeparate(desiredVariable);
            this.startExecution();//the command can stop execution if an return of break are used
            this.pushReturnValueIfNew();
            return result
        }
        if (desiredVariable instanceof ModuleCommand) {
            var result = this.runModuleCommand(desiredVariable);
            this.pushReturnValueIfNew();
            return result
        }
        this.pushToStack(desiredVariable);
        if (desiredVariable === undefined) {
            this.showInfo("\nVariable is undefined.\n");
        } else if (desiredVariable === null) {
            this.showInfo("\nVariable is null.\n");
        }
        return RUNTIME_OK;
    }

    getVariable(propertyList, isLocal) {
        var desiredVariable;
        if (isLocal) {
            desiredVariable = this.localVarList[this.currentLocalMemory][propertyList[0]];
        } else {
            //Checks for special tokens
            switch (propertyList[0]) {
                case "stack":
                    desiredVariable = this.getStack();
                    break;
                default:
                    desiredVariable = this.globalVarList[propertyList[0]];
            }
        }
        var length = propertyList.length;
        for (var i = 1; i < length; i++) {
            if (desiredVariable === undefined || desiredVariable === null) {
                return new RuntimeError("Undefined/null objects don't contain any properties");
            }
            desiredVariable = desiredVariable[propertyList[i]];
        }
        return desiredVariable;
    }

// e.g.  setVariableByTokenString("$var1.x", 43) makes the x property of the local variable var1 become 43
    setVariableByTokenString(variableToken, obj) {
        //TODO This(will use the parser);
    }

    setVariable(variableToken, obj) {
        var propertyList = TokenTypeList.VARIABLE.generatePropertyList(variableToken, this);
        var varId = propertyList.pop();
        if (propertyList.length != 0) {
            var parent = this.getVariable(propertyList, variableToken.isLocal);
            if (parent instanceof RuntimeError) {
                return parent;
            }
            if (parent === undefined || parent === null) {
                return new RuntimeError("Undefined/null objects don't contain any properties");
            }
            parent[varId] = obj;
        } else {
            if (variableToken.isLocal) {
                this.localVarList[this.currentLocalMemory][varId] = obj;
            } else {
                //Checks for special tokens
                switch (varId) {
                    case "stack":
                        return new RuntimeError("Cannot override the stack object.");
                    default:
                        this.globalVarList[varId] = obj;
                }
            }
        }
        return RUNTIME_OK;
    }

    getStack() {
        return this.stack;
    }

    getStackLength() {
        return this.getStack().length - this.currentStackLock;
    }

    getStackTop() {
        if (this.getStackLength() > 0) {
            return this.getStack()[this.getStackLength() - 1];
        }
        return new RuntimeError("No Top on am empty stack.");
    }

    lockStack() {
        this.stackLockList.push(this.currentStackLock);
        this.currentStackLock = this.stack.length;
    }

    unlockStack() {
        this.popAllFromStack();
        if (this.stackLockList.length > 0) {
            this.currentStackLock = this.stackLockList.pop();
        }
    }

    runModuleCommand(command) {
        var callbackNum = command.callbackList.length;
        for (var i = 0; i < callbackNum; i++) {
            var callbackObject = command.callbackList[i];
            if (this.checkCallbackArguments(callbackObject)) {
                if (callbackObject.argumentList) {
                    return callbackObject.callback(this, ...this.popMultipleFromStack(callbackObject.argumentList.length));
                }
                return callbackObject.callback(this);
            }
        }
        return new RuntimeError("No valid callback found for command '" + command.id + "'.");
    }

    checkCallbackArguments(callbackObject) {
        var args = callbackObject.argumentList;
        if (args) {
            if (args.length > this.getStackLength()) {
                return false;
            }
            var argsNum = args.length;
            //This is different from the getStackLength because the getStackLength calculates starting from the current stack lock
            var stackSize = this.stack.length;
            for (var i = 1; i <= argsNum; i++) {
                if (!this.checkIfValueIsValid(args[argsNum - i].type, this.stack[stackSize - i])) {
                    return false;
                }
            }
        }
        return true;
    }

    checkIfValueIsValid(typeObj, value) {
        if (typeObj.multi) {
            if (value instanceof Array) {
                if (!typeObj.isAny) {
                    var length = value.length;
                    if (typeObj.checkByInstanceOf) {
                        for (var i = 0; i < length; i++) {
                            if (!(value instanceof typeObj.value)) {
                                return false;
                            }
                        }
                    } else {
                        for (var i = 0; i < length; i++) {
                            if (!(typeof value == typeObj.value)) {
                                return false;
                            }
                        }
                    }
                }
            }
        } else {
            if (!typeObj.isAny) {
                if (typeObj.checkByInstanceOf) {
                    return (value instanceof typeObj.value);
                } else {
                    return (typeof value == typeObj.value);
                }
            }
        }
        return true;
    }

    setReturnValue(returnValue) {
        this.isReturnValueNew = true;
        this.returnValue = returnValue;
    }

    getReturnValue() {
        this.isReturnValueNew = false;
        return this.returnValue;
    }

    pushReturnValueIfNew() {
        if (this.isReturnValueNew) {
            this.pushToStack(this.getReturnValue());
        }
    }
}
