class Command {
    constructor(tokenList) {
        this.tokenList = tokenList;
    }

    run(engine) {
        return engine.runCommandList(this.tokenList);
    }
}

class UserCommand {
    constructor(command) {
        this.command = command;
    }

    run(engine) {
        return this.command.run(engine);
    }
}

class ModuleCommand {
    constructor(name, id, callbackList) {
        this.name = name;
        this.id = id;
        this.callbackList = callbackList;
        this.writeProtected = true;
    }
}

class Module {
    constructor(name, loader,commandList) {
        this.name = name;
        this.loadModule = loader;
        this.commandList = commandList;
    }
}