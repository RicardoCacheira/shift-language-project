var main = this;

var moduleList = [];

const types = {
    WEB: 0,
    NODE: 1
}

const mainFileList = [
    "utils/js/utils.js",
    "engine/js/token/tokenUtils.js",
    "engine/js/token/lexerProcessorFunc.js",
    "engine/js/token/parseProcessorFunc.js",
    "engine/js/token/execFunc.js",
    "engine/js/token/tokenList.js",
    "engine/js/parser/lexer.js",
    "engine/js/parser/parser.js",
    "engine/js/i4classes.js",
    "engine/js/i4engine.js"
]

const webFileList = [
    "web/js/webConsole.js",
    "web/js/webInterpreter.js"
]

const moduleFileList = [
    "modules/js/ioModule.js",
    "modules/js/arithmeticModule.js"
]

var engine;

function loadEngine(option) {

    if (main instanceof Window) {
        var head = document.head;
        mainFileList.forEach(function (element) {
            loadFileInWeb(element, head);
        });
        webFileList.forEach(function (element) {
            loadFileInWeb(element, head);
        });
        moduleFileList.forEach(function (element) {
            loadFileInWeb(element, head);
        });
    }

}


function loadFileInWeb(file, head) {
    var element = document.createElement("script");
    element.setAttribute("type", "text/javascript");
    element.setAttribute("src", file);
    element.setAttribute("charset", "UTF-8");
    head.appendChild(element);

}

function startEngine() {
    engine = new I4Engine();
    engine.addOutput(simpleOutput);
}