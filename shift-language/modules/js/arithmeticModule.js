(function () {
    moduleList.push(new Module("arithmeticModule", loadModule, [
            new ModuleCommand(
                "addition",
                "+",
                [
                    {
                        description: "Adds both operands.",
                        argumentList: [
                            {
                                name: "operand1",
                                type: {multi: false, isAny: true}
                            },
                            {
                                name: "operand2",
                                type: {multi: false, isAny: true}
                            }
                        ],
                        returnType: "number",
                        returnDescription: "The result of the addition of both operands.",
                        notes: [
                            "If the first operand is a string, then the result will be the concatenation of the string and the string representation of the second operand."
                        ],
                        callback: additionCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "subtraction",
                "-",
                [
                    {
                        description: "Subtracts the second operand from the first.",
                        argumentList: [
                            {
                                name: "operand1",
                                type: {multi: false, isAny: true}
                            },
                            {
                                name: "operand2",
                                type: {multi: false, isAny: true}
                            }
                        ],
                        returnType: "number",
                        returnDescription: "The result of the subtraction of the first operand with the second.",
                        notes: [
                            "If any of the operands is not a number it will convert it."
                        ],
                        callback: subtractionCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "multiplication",
                "*",
                [
                    {
                        description: "Multiplies both operands.",
                        argumentList: [
                            {
                                name: "operand1",
                                type: {multi: false, isAny: true}
                            },
                            {
                                name: "operand2",
                                type: {multi: false, isAny: true}
                            }
                        ],
                        returnType: "number",
                        returnDescription: "The result of the multiplication of both operands.",
                        notes: [
                            "If any of the operands is not a number it will convert it."
                        ],
                        callback: multiplicationCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "division",
                "/",
                [
                    {
                        description: "Divides the first operand with the second.",
                        argumentList: [
                            {
                                name: "operand1",
                                type: {multi: false, isAny: true}
                            },
                            {
                                name: "operand2",
                                type: {multi: false, isAny: true}
                            }
                        ],
                        returnType: "number",
                        returnDescription: "The result of the division of the first operand with the second.",
                        notes: [
                            "If any of the operands is not a number it will convert it."
                        ],
                        callback: divisionCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "exponent",
                "^",
                [
                    {
                        description: "Returns the exponent of the first operand with the second.",
                        argumentList: [
                            {
                                name: "operand1",
                                type: {multi: false, isAny: true}
                            },
                            {
                                name: "operand2",
                                type: {multi: false, isAny: true}
                            }
                        ],
                        returnType: "number",
                        returnDescription: "The result of the exponent of the first operand with the second.",
                        notes: [
                            "If any of the operands is not a number it will convert it."
                        ],
                        callback: exponentCallback0
                    }
                ]
            )
        ])
    );

    function loadModule(engine) {
        this.commandList.forEach(function (command) {
            engine.addModuleCommand(command);
        });
    }

    function additionCallback0(engine, operand1, operand2) {
        engine.setReturnValue(operand1 + operand2);
        return RUNTIME_OK;
    }

    function subtractionCallback0(engine, operand1, operand2) {
        engine.setReturnValue(operand1 - operand2);
        return RUNTIME_OK;
    }

    function multiplicationCallback0(engine, operand1, operand2) {
        engine.setReturnValue(operand1 * operand2);
        return RUNTIME_OK;
    }

    function divisionCallback0(engine, operand1, operand2) {
        engine.setReturnValue(operand1 / operand2);
        return RUNTIME_OK;
    }

    function exponentCallback0(engine, operand1, operand2) {
        engine.setReturnValue(operand1 ** operand2);
        return RUNTIME_OK;
    }
}());