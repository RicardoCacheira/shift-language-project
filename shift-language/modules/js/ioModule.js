(function () {
    moduleList.push(new Module("ioModule", loadModule, [
            new ModuleCommand(
                "in",
                "in",
                [
                    {
                        description: "Gets a input and stores it in the stack.",
                        returnType: "string",
                        returnDescription: "The inputted string.",
                        callback: inCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "out",
                "out",
                [
                    {
                        description: "Outputs the string of the given value.",
                        argumentList: [
                            {
                                name: "obj",
                                type: {multi: false, isAny: true}
                            }
                        ],
                        callback: outCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "outColor",
                "outColor",
                [
                    {
                        description: "Outputs the string of the given value with the given color(in hex).",
                        argumentList: [
                            {
                                name: "obj",
                                type: {multi: false, isAny: true}
                            },
                            {
                                name: "hex",
                                type: {checkByInstanceOf: false, multi: false, isAny: false, value: "string"}
                            }
                        ],
                        callback: outColorCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "ln",
                "ln",
                [
                    {
                        description: "Outputs a new line character.",
                        callback: lnCallback0
                    }
                ]
            ),
            new ModuleCommand(
                "inCheck",
                "inCheck",
                [
                    {
                        description: "Gets a input, checks the return of the checker command and if true, stores it in the stack, if false, runs the continue command and if that one returns true restarts the process.",
                        argumentList: [
                            {
                                name: "startCommand",
                                type: {checkByInstanceOf: true, multi: false, isAny: false, value: Command}
                            },
                            {
                                name: "checkerCommand",
                                type: {checkByInstanceOf: true, multi: false, isAny: false, value: Command}
                            },
                            {
                                name: "continueCommand",
                                type: {checkByInstanceOf: true, multi: false, isAny: false, value: Command}
                            }
                        ],
                        returnType: "String",
                        returnDescription: "The inputted string, only if the checker command returns true.",
                        notes: [
                            "The continue command can be used to send warning messages to te user."
                        ],
                        callback: inCheckCallback0
                    }
                ]
            )
        ])
    );

    function loadModule(engine) {
        this.commandList.forEach(function (command) {
            engine.addModuleCommand(command);
        });
    }

    function inCallback0(engine) {
        engine.setReturnValue(engine.requestInput());
        return RUNTIME_OK;
    }

    function outCallback0(engine, obj) {
        engine.plainOutput(obj);
        return RUNTIME_OK;
    }

    function outColorCallback0(engine, obj, hex) {
        engine.customOutput(obj, hex);
        return RUNTIME_OK;
    }

    function lnCallback0(engine) {
        engine.plainOutput("\n");
        return RUNTIME_OK;
    }

    function inCheckCallback0(engine, startCommand, checkerCommand, continueCommand) {
        engine.setReturnValue(result);
        return RUNTIME_OK;
    }
}());