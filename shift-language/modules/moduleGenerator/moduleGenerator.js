var fs = require("fs");

var list = process.argv.slice(2);
var location = list.shift();
generateModules(list);

function generateModules(moduleList) {
    moduleList.forEach(function (module) {
        var data = fs.readFileSync(module);
        generateModule(JSON.parse(data));
    });
}

function generateModule(json) {
    var result = "(function () {\n\tmoduleList.push(new Module(\"" + json.name + "\", loadModule, ["
    json.commandList.forEach(function (command) {
        result += "\n\t\t\tnew ModuleCommand(\n\t\t\t\t\"" + command.name + "\",\n\t\t\t\t\"" + command.id + "\",\n\t\t\t\t["

        var length = command.callbackList.length;
        for (var i = 0; i < length; i++) {
            var callback = command.callbackList[i];
            result += "\n\t\t\t\t\t{\n\t\t\t\t\t\tdescription: \"" + callback.description + "\",";
            if (callback.argumentList != undefined) {
                result += "\n\t\t\t\t\t\targumentList: ["
                callback.argumentList.forEach(function (argument) {
                    result += "\n\t\t\t\t\t\t\t{\n\t\t\t\t\t\t\t\tname: \"" + argument.name + "\",\n\t\t\t\t\t\t\t\ttype: " + argumentTypeGenerator(argument.type) + "\n\t\t\t\t\t\t\t},";
                });
                result = result.substring(0, result.length - 1);
                result += "\n\t\t\t\t\t\t],";
            }
            if (callback.returnType != undefined) {
                result += "\n\t\t\t\t\t\treturnType: \"" + callback.returnType + "\","
            }
            if (callback.returnDescription != undefined) {
                result += "\n\t\t\t\t\t\treturnDescription: \"" + callback.returnDescription + "\","
            }
            if (callback.notes != undefined) {
                result += "\n\t\t\t\t\t\tnotes: ["
                callback.notes.forEach(function (note) {
                    result += "\n\t\t\t\t\t\t\t\"" + note + "\",";
                });
                result = result.substring(0, result.length - 1);
                result += "\n\t\t\t\t\t\t],";
            }
            result += "\n\t\t\t\t\t\tcallback: " + command.name + "Callback" + i + "\n\t\t\t\t\t}"
        }
        result += "\n\t\t\t\t]\n\t\t\t),"
    });
    result = result.substring(0, result.length - 1);

    result += "\n\t\t])\n\t);\n\n\tfunction loadModule(engine) {\n\t\tthis.commandList.forEach(function (command) {\n\t\t\tengine.addModuleCommand(command);\n\t\t});\n\t}";

    json.commandList.forEach(function (command) {
        var lenght = command.callbackList.length;
        for (var i = 0; i < lenght; i++) {
            var callback = command.callbackList[i];
            result += "\n\n\tfunction " + command.name + "Callback" + i + "(engine";
            if (callback.argumentList) {
                callback.argumentList.forEach(function (argument) {
                    result += ", " + argument.name;
                });
            }
            result += ") {\n";
            if (callback.returnType) {
                result += "\t\tengine.setReturnValue(result);\n";
            }
            result += "\t\treturn RUNTIME_OK;\n\t}";
        }
    });

    result += "\n}());";


    console.log(location + json.name + ".js");
    fs.writeFile(location + json.name + ".js", result, function (err) {
        if (err) {
            return console.error(err);
        }
        console.log("Module \"" + json.name + "\" created.");
    });
}

/*
 Uppercase - check with instanceof
 lowercase - check with typeof
 * - multiple values

 */
function argumentTypeGenerator(type) {

    var final = "{";
    var value = type;
    var multi;
    if (value.charAt(0) == '*') {
        value = value.substring(1);
        multi = true;
    } else {
        multi = false;
    }
    if (value == "ANY") {
        final += "multi: " + multi + ", isAny: true";
    }
    else if (value.charAt(0) == value.charAt(0).toLowerCase()) {
        final += "checkByInstanceOf: false, multi: " + multi + ", isAny: false, value: \"" + value + "\"";
    } else {
        final += "checkByInstanceOf: true, multi: " + multi + ", isAny: false, value: " + value + "";
    }
    final += "}";

    return final;
}


